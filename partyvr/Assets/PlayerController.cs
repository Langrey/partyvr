﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;
public class PlayerController : NetworkBehaviour {

	float xInput;
	float zInput;

	// Use this for initialization
	void Start () {
		
		if(!isLocalPlayer){
			GetComponentInChildren<Camera>().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update (){
		if(!isLocalPlayer){
			return;
		}

		xInput = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		zInput = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		transform.Rotate(0, xInput, 0);
		transform.Translate(0, 0, zInput);
			
	}
}
