﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.XR;
public class CustomNetworkManager : NetworkManager 
 {

     public Transform spawnPosition;   
     public int curPlayer;

    void Start(){
        XRSettings.enabled = false;
    }

     //Called on client when connect
     public override void OnClientConnect(NetworkConnection conn) {      
		 if(NetworkServer.connections.Count > 0){
			 curPlayer = 0;
             XRSettings.enabled = true;
		 }
		 else{
			 curPlayer = 1;
		 }
		 
         // Create message to set the player
         IntegerMessage msg = new IntegerMessage(curPlayer);      

         // Call Add player and pass the message
         ClientScene.AddPlayer(conn,0, msg);
     }
  
 // Server
     public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader ) { 
         // Read client message and receive index
		 Debug.Log("NEW MESSAGE");
         if (extraMessageReader != null) {
             var stream = extraMessageReader.ReadMessage<IntegerMessage> ();
             curPlayer = stream.value;
			 Debug.Log("MESSAGE: " + curPlayer);
         }
         //Select the prefab from the spawnable objects list
         var playerPrefab = spawnPrefabs[curPlayer];       
  
         // Create player object with prefab
         var player = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;        
         
         // Add player object for connection
         NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
     }
 }
