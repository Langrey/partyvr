﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyPlayer : NetworkLobbyPlayer {

    private void Start()
    {
        Camera playerCam = GetComponentInChildren<Camera>();
        //Setting non-local lobby player
        if (!isLocalPlayer)
        {
            if(playerCam != null)
            {
                //Disable Vr components
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
                //Disable Camera for non local player
                GetComponentInChildren<Camera>().enabled = false;

				Quaternion rotation = Quaternion.Euler (45, -45, 0);
				Instantiate(GameObject.Find("LobbyManager").GetComponent<CustomLobbyManager>().LobbyCamera, new Vector3(5,4,-5), rotation);
                

            }
        }
    }


    void Update() {
        
            if(!isLocalPlayer)
            {
			return;
		    }

		float xInput = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		float zInput = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		transform.Rotate(0, xInput, 0);
		transform.Translate(0, 0, zInput);
        
    }
}
