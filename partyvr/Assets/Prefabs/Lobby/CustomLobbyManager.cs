﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.XR;

public class CustomLobbyManager : NetworkLobbyManager {
	//are we host
    public GameObject LobbyCamera;

    

    private int prefabIndex = 0;

    public void ToggleVR()
    {
        if (XRSettings.loadedDeviceName == "openvr") {
            StartCoroutine(LoadDevice("None"));
        } else {
            StartCoroutine(LoadDevice("openvr"));
        }
        Debug.Log(XRSettings.loadedDeviceName);
    }

    IEnumerator LoadDevice(string newDevice)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = true;
    }

     void Awake() {
    }
        


    public void HostGame()
    {
        //Host is VRPREFAB
        StartHost();

        
        
    }

    public void JoinGame()
    {
        //Client is normal prefab
        StartClient();
  
    }


    //Creating lobbyplayer gameobject
    //first function call is for host so after that we change index to 1 so every next player
    //is normal player
    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {

        GameObject lobbyPlayer = Instantiate(spawnPrefabs[prefabIndex]);

        if(prefabIndex == 0)
            prefabIndex = 1;

        return lobbyPlayer;
    }

    public override void OnClientConnect(NetworkConnection conn){
        base.OnClientConnect(conn);

        

    }
}
